profile_qemu_x86_64() {
	viryaos_grub_mod="part_gpt fat ext2 iso9660 gzio linux acpi normal cpio crypto disk boot crc64 gpt search_disk_uuid tftp verify xzio xfs video multiboot2 gfxterm efi_gop efi_uga"

	arch="x86_64"
	output_format="qemu_x86_64_img"

	kernel_flavors="qemu_x86_64-dom0"

	# --extra-repository '@apkrepo-dom0-qemu_x86_64 ...' in `README.md`
	kernel_flavors_repo="@apkrepo-dom0-qemu_x86_64"

	kernel_cmdline="console=hvc0"

	initfs_features="ata base bootchart cdrom squashfs ext2 ext3 ext4 scsi"
	initfs_cmdline="modules=loop,squashfs rootfs_cpio"

	viryaos_rel_ver="2018.09.0"
}

create_image_qemu_x86_64_img() {
	set -e

	# startup.nsh
	echo "\\EFI\\ViryaOS\\grubx64.efi" > ${DESTDIR}/startup.nsh

	local IMG="${OUTDIR}/qemu_x86_64.img"

	# Both GPT partitions and VFAT filesystem uses 512 as the default
	# sector size
	local _sector_size=512

	local _esp_file="${DESTDIR}/esp.img"
	# For now we assume 2048 KiB, which is 4096 sectors
	# Currently `grubx86.efi` is 995 sectors (495.5 KiB)
	local _esp_file_size_kib=2048

	mkfs.vfat -v -C $_esp_file $_esp_file_size_kib
	mmd -i $_esp_file ::/EFI
	mmd -i $_esp_file ::/EFI/ViryaOS
	mcopy -i $_esp_file -s ${DESTDIR}/viryaos-grub-efi/grubx64.efi ::/EFI/ViryaOS/
	mcopy -i $_esp_file -s ${DESTDIR}/viryaos-grub-cfg/grub.cfg ::/EFI/ViryaOS/
	mcopy -i $_esp_file -s ${DESTDIR}/startup.nsh ::/

	# 64 MiB
	truncate $IMG -s 64M

	# create GPT partition table
	sgdisk -og $IMG

	local _esp_file_size_bytes=$(( $_esp_file_size_kib * 1024 ))
	local _esp_file_size_sectors=$(( $_esp_file_size_bytes / $_sector_size ))

	# start ESP at sector 2048
	local _esp_sector_start=2048
	local _esp_sector_end=$(( $_esp_sector_start + $_esp_file_size_sectors - 1 ))
	# create EFI System Partition
	sgdisk -n 1:$_esp_sector_start:$_esp_sector_end -c 1:"EFI System Partition" -t 1:ef00 $IMG

	# copy ESP file over to IMG
	dd if=$_esp_file of=$IMG bs=$_sector_size count=$_esp_file_size_sectors conv=notrunc seek=$_esp_sector_start

	# usable_sector_first is obtained by running
	# ```
	# sgdisk -F /tmp/img/qemu_x86_64.img
	# ```
	local _usable_sector_first=6144


	# usable_sector_last is obtained by running
	# ```
	# sgdisk -E /tmp/img/qemu_x86_64.img
	# ```
	local _usable_sector_last=131038

	# create linux partition
	sgdisk -n 2:$_usable_sector_first:$_usable_sector_last -c 2:"Linux" -t 2:8300 $IMG

	# find the first available loop device
	local _loop_dev=$(sudo losetup -f)

	# attach loopback device to $IMG
	sudo losetup $_loop_dev $IMG

	local _disksize=$(sudo blockdev --getsize $_loop_dev)

	sudo dmsetup create diskimage --table "0 $_disksize linear $_loop_dev 0"

	# ensure that /dev/mapper/diskimage exists
	while [ ! -b /dev/mapper/diskimage ]
	do
		sleep 2
	done

	sudo kpartx -a /dev/mapper/diskimage

	# ensure that /dev/mapper/diskimage1 and  /dev/mapper/diskimage2 exists
	while [ ! -b /dev/mapper/diskimage1 ] || [ ! -b /dev/mapper/diskimage2 ]
	do
		sleep 2
	done

	# format vos_a partition
	# NOTE: vos_a and vos_b will have a dependency with initramfs-init script
	sudo mkfs.ext4 -L vos_a -F /dev/mapper/diskimage2

	# create mount point and mount vos_a
	mkdir -p ${DESTDIR}/part/vos_a
	sudo mount -t ext4 /dev/mapper/diskimage2 $DESTDIR/part/vos_a

	# Copy the required files over
	local _vmlinuz=$(basename ${WORKDIR}/kernel_*/boot/vmlinuz-*)
	local _fullkver=${_vmlinuz#vmlinuz-}

	sudo cp ${DESTDIR}/xen.gz ${DESTDIR}/part/vos_a
	sudo cp ${DESTDIR}/boot/vmlinuz-${_fullkver} ${DESTDIR}/part/vos_a
	sudo cp ${DESTDIR}/boot/initramfs-${_fullkver} ${DESTDIR}/part/vos_a
	sudo cp ${DESTDIR}/boot/modloop-${_fullkver} ${DESTDIR}/part/vos_a

	sudo cp ${DESTDIR}/rootfs.cpio.gz ${DESTDIR}/part/vos_a

	# unmount
	sudo sync
	sudo fstrim $DESTDIR/part/vos_a
	sudo umount $DESTDIR/part/vos_a
	sudo kpartx -d /dev/mapper/diskimage
	sudo dmsetup remove diskimage
	sudo losetup -d $_loop_dev
}

section_rootfs_qemu_x86_64() {
	build_section rootfs_qemu_x86_64 $(echo "rootfs_qemu_x86_64" | checksum)
}

build_rootfs_qemu_x86_64() {
	local _script="${PWD}/genrootfs_qemu_x86_64-dom0.sh"
	$_script -a x86_64 -r "$APKROOT/etc/apk/repositories" -k /etc/apk/keys -o "$DESTDIR" -v "$viryaos_rel_ver"
}

# adapted from upstream `grub_gen_config()`
viryaos_grub_gen_config() {
	local _vmlinuz=$(basename ${WORKDIR}/kernel_*/boot/vmlinuz-*)
	local _fullkver=${_vmlinuz#vmlinuz-}

	cat <<- EOF

	set timeout=1
	set gfxpayload=text

	menuentry "Linux $_fullkver" {
		insmod part_gpt
		insmod ext2

		set root=(hd0,gpt2)

		multiboot2 /xen.gz placeholder console=com1 com1=115200,8n1 clocksource=pit
		module2    /vmlinuz-$_fullkver $initfs_cmdline $kernel_cmdline
		module2    /initramfs-$_fullkver
	}
	EOF
}

# adapted from upstream `build_grub_cfg()`
build_viryaos_grub_cfg() {
	local grub_cfg="$1"
	mkdir -p "${DESTDIR}/$(dirname $grub_cfg)"
	viryaos_grub_gen_config > "${DESTDIR}"/$grub_cfg
}

# adapted from upstream `build_grub_efi()`
build_viryaos_grub_efi() {
	local _format="$1"
	local _efi="$2"

	# Prepare grub-efi bootloader
	mkdir -p "$DESTDIR/viryaos-grub-efi"

	/usr/local/viryaos-grub/x86_64/bin/grub-mkimage \
		--format="$_format" \
		--output="$DESTDIR/viryaos-grub-efi/$_efi" \
		--prefix="/EFI/ViryaOS" \
		$viryaos_grub_mod
}

# adapted from upstream `section_grub_efi()`
section_viryaos_grub_efi() {
	[ -n "$viryaos_grub_mod" ] || return 0

	local _format _efi
	case "$arch" in
	aarch64)_format="arm64-efi";  _efi="grubaa64.efi" ;;
	x86_64)	_format="x86_64-efi"; _efi="grubx64.efi"  ;;
	*)	return 0 ;;
	esac

	build_section viryaos_grub_cfg viryaos-grub-cfg/grub.cfg $(viryaos_grub_gen_config | checksum)
	build_section viryaos_grub_efi $_format $_efi $(echo "viryaos_grub_efi" | checksum)
}
