#!/bin/sh

cleanup() {
	sudo rm -rf "$tmp"
}

tmp="$(mktemp -d /tmp/genrootfs_qemu_x86_64-dom0.XXXXXX)"
trap cleanup EXIT

ovl_dir="$(realpath $(dirname $0))/genrootfs_qemu_x86_64-dom0-ovl"

while getopts "a:k:o:r:v:" opt; do
	case $opt in
	a) arch="$OPTARG";;
	k) keys_dir="$OPTARG";;
	o) out_dir="$OPTARG";;
	r) repositories_file="$OPTARG";;
	v) rel_ver="$OPTARG";;
	esac
done
shift $(( $OPTIND - 1))

if [ -z "$arch" ] || [ -z "$keys_dir" ] || [ -z "$out_dir" ] || [ -z "$repositories_file" ] || [ -z "$rel_ver" ]; \
then
	echo "-a, -k, -o, -r, -v options needed"
	exit 1
fi

apk_pkgs="
	alpine-baselayout
	apk-tools
	musl
	openrc

	busybox
	busybox-initscripts

	sudo
	shadow@apkrepo-dom0-qemu_x86_64

	dbus

	bash
	python2
	gettext
	zlib
	ncurses
	texinfo
	yajl
	libaio
	xz-dev
	util-linux
	argp-standalone
	"
apks=""
for i in $apk_pkgs; do
	apks="$apks $i"
done

tmprootfs="$tmp/rootfs"

abuild-apk add --arch "$arch" --keys-dir "$keys_dir" --no-cache \
	--repositories-file "$repositories_file" \
	--root "$tmprootfs" --initdb

# install xen
sudo sh -c "cd $tmprootfs; tar xvzf /home/builder/viryaos-xen.tar.gz"

# dbus post-install script requires /dev/urandom
sudo mknod -m 644 "$tmprootfs/dev/urandom" c 1 9

abuild-apk add --arch "$arch" --keys-dir "$keys_dir" --no-cache \
	--repositories-file "$repositories_file" \
	--root "$tmprootfs" $apks

sudo rm -f  "$tmprootfs/dev/urandom"

# setup openrc
runlevel_sysinit="
	devfs
	dmesg
	mdev
	"
for i in $runlevel_sysinit; do
	sudo sh -c "ln -sf /etc/init.d/$i $tmprootfs/etc/runlevels/sysinit/$i"
done

sudo sh -c "echo viryaos-qemu_x86_64-dom0 >> $tmprootfs/etc/hostname"

runlevel_boot="
	sysctl
	hostname
	bootmisc
	syslog
	urandom
	networking
	"
for i in $runlevel_boot; do
	sudo sh -c "ln -sf /etc/init.d/$i $tmprootfs/etc/runlevels/boot/$i"
done

runlevel_default="
	acpid
	crond
	dbus
	local
	xenconsoled
	xenstored
	"
for i in $runlevel_default; do
	sudo sh -c "ln -sf /etc/init.d/$i $tmprootfs/etc/runlevels/default/$i"
done

runlevel_shutdown="
	killprocs
	mount-ro
	savecache
	"
for i in $runlevel_shutdown; do
	sudo sh -c "ln -sf /etc/init.d/$i $tmprootfs/etc/runlevels/shutdown/$i"
done

# setup vos-user account
# echo "vos-user" | openssl passwd -1 -stdin
# $1$xTMmPsRW$rzaGmlHqkmaOwGMQW9tl6/
# NOTE: There is a `\` before the `$` in the hashed password. This is so that
# shell escapes are correctly handled
sudo sh -c "chroot $tmprootfs /usr/sbin/groupadd -g 500 vos-user"
sudo sh -c "chroot $tmprootfs /usr/sbin/useradd -d /home/vos-user -g vos-user -s /bin/ash -G wheel -m -N -u 500 vos-user -p '\$1\$xTMmPsRW\$rzaGmlHqkmaOwGMQW9tl6/'"

# setup /etc/sudoers.d/vos-user
sudo sh -c "cp $ovl_dir/etc-sudoers.d-vos-user $tmprootfs/etc/sudoers.d/vos-user"
sudo sh -c "chown root:root $tmprootfs/etc/sudoers.d/vos-user"
sudo sh -c "chmod 440 $tmprootfs/etc/sudoers.d/vos-user"

# disable root password
sudo sh -c "sed -i -e 's/^root.*$/root:*LOCK*:14600::::::/' $tmprootfs/etc/shadow"

# update /etc/motd
sudo sh -c "cat $ovl_dir/etc-motd > $tmprootfs/etc/motd"
sudo sh -c "sed -i -e s/RELEASE_VERSION/${rel_ver}/ $tmprootfs/etc/motd"

# update /etc/os-release
sudo sh -c "cat $ovl_dir/etc-os-release > $tmprootfs/etc/os-release"
sudo sh -c "sed -i -e s/RELEASE_VERSION/${rel_ver}/ $tmprootfs/etc/os-release"

# setup networking
sudo sh -c "cp $ovl_dir/etc-network-interfaces $tmprootfs/etc/network/interfaces"
sudo sh -c "chown root:root $tmprootfs/etc/network/interfaces"
sudo sh -c "chmod 644 $tmprootfs/etc/network/interfaces"

# setup inittab
sudo sh -c "cp $ovl_dir/etc-inittab $tmprootfs/etc/inittab"
sudo sh -c "chown root:root $tmprootfs/etc/inittab"
sudo sh -c "chmod 644 $tmprootfs/etc/inittab"

sudo sh -c "mv $tmprootfs/boot/xen* $out_dir/"
sudo sh -c "cd $tmprootfs; find . | cpio -H newc -o | gzip > $tmp/rootfs.cpio.gz"

cp $tmp/rootfs.cpio.gz $out_dir/rootfs.cpio.gz
