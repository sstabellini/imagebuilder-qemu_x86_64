# imagebuilder

This docker image has `mkimage.sh` setup to generate `qemu_x86_64.img` image.

To get started

```
$ cd imagebuilder/scripts/

$ docker build -t image-builder-qemu_x86_64 .
```

Go to the directory containing `ViryaOS` tree.

```
$ docker run --rm --privileged=true -ti -v $(pwd):/home/builder/src -v /dev:/dev -v /tmp:/tmp \
       image-builder-qemu_x86_64 /bin/su -l -s /bin/sh builder

e30e9cd62ad3:~$ sudo apk update

e30e9cd62ad3:~$ cd src/imagebuilder/scripts/

e30e9cd62ad3:~/src/imagebuilder/scripts$ mkdir /tmp/img

e30e9cd62ad3:~/src/imagebuilder/scripts$ ./mkimage.sh \
  --tag qemu_x86_64 \
  --outdir /tmp/img \
  --arch x86_64 \
  --repository http://dl-cdn.alpinelinux.org/alpine/v3.7/main \
  --extra-repository '@apkrepo-dom0-qemu_x86_64 /home/builder/apkrepo/dom0-qemu_x86_64/v3.7/main' \
  --profile qemu_x86_64
```

The generated disk image is at `/tmp/img/qemu_x86_64.img`.
